import React from "react";
import Svg from "../Svg";
import { SvgProps } from "../types";

const Icon: React.FC<SvgProps> = (props) => {
  return (
    <Svg
      viewBox="0 0 1300 200"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      {...props}
    >
      <title>Group 11 Copy</title>
      <defs>
        <filter
          x="-17.3%"
          y="-14.2%"
          width="134.6%"
          height="128.5%"
          filterUnits="objectBoundingBox"
          id="prefix__a"
        >
          <feOffset dy={4} in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation={4.5}
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0"
            in="shadowBlurOuter1"
            result="shadowMatrixOuter1"
          />
          <feMerge>
            <feMergeNode in="shadowMatrixOuter1" />
            <feMergeNode in="SourceGraphic" />
          </feMerge>
        </filter>
        <filter
          x="-1.4%"
          y="-5.7%"
          width="102.8%"
          height="115.3%"
          filterUnits="objectBoundingBox"
          id="prefix__c"
        >
          <feOffset dy={4} in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation={4.5}
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="230.913%" id="prefix__e">
          <stop stopColor="#FF9B15" offset="0%" />
          <stop stopOpacity={0.5} offset="100%" />
        </linearGradient>
        <style>
        @import url(`http://fonts.cdnfonts.com/css/lion-king`);
        </style>
        <text
          id="prefix__d"
          fontFamily="Lion-kinG, Lion kinG"
          fontSize={180.795}
          fontWeight="normal"
        >
          <tspan className='icon-text' x={183} y={164}>
            AfricaSwap
          </tspan>
        </text>
        <path
          d="M12.03 20.563l108.643 14.386M3.795 41.448l125.063 85.624M2.108 54.048l147.486 29.418M18.678 79.894l101.995-38.446M55.537 93.934L110.7 27.756M67.988 167.404L115.067 31.08m-82.5 50.7L63.97.025M45.813 81.78l42.168-64.541M76.967 185.213l53.776-74.313m-34.577 71.039l-26.491-28.872m34.18 26.193l-35.867-61.514m40.977 49.757L63.97 129.304m53.23 34.478l-50.849-60.67m66.277 17.015L63.97 21.952m60.324 118.812L72.354 20.563M7.268 79.894L36.09 7.466m81.11 149.917L43.482 9.55m97.53 86.765L18.678 17.239m127.245 70.097L76.967 17.239M61.291 99.242l63.003-52.536M53.007 88.874l96.587-25.648M64.664 110.9l67.964-5.21M11.832 27.21L64.665 9.55M5.779 34.403l45.045 45.491m-50.8-19.05L117.2 17.24M64.665 138.383l59.629-84.335m-28.128-36.81l56.256 64.542M76.967 174.3l55.661-113.604"
          id="prefix__b"
        />
      </defs>
      <g fill="none" fillRule="evenodd">
        <g
          filter="url(#prefix__a)"
          transform="translate(9 4.614)"
          fillRule="nonzero"
        >
          <use
            stroke="#F95406"
            strokeWidth={3}
            fill="#FF9B15"
            xlinkHref="#prefix__b"
          />
          <use stroke="#FF9B15" strokeWidth={2.205} xlinkHref="#prefix__b" />
        </g>
        <g transform="translate(9 -2)">
          <use fill="#ff9b15" filter="url(#prefix__c)" xlinkHref="#prefix__d" />
          <use fill="#ff9b15" xlinkHref="#prefix__d" />
          <use fill="url(#prefix__e)" xlinkHref="#prefix__d" />
          <use fill="#ff9b15" xlinkHref="#prefix__d" />
        </g>
      </g>
    </Svg>
  );
};

export default Icon;
