import React from "react";
import Svg from "../../../components/Svg/Svg";
import { SvgProps } from "../../../components/Svg/types";

const Icon: React.FC<SvgProps> = (props) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
	  {...props}
    >
      <path
        fill="#ff9b15"
        d="M15.602 14.398v-4.32h3.597V7.2L24 12.24l-4.8 5.042v-2.883h-3.598m-1.204-9.07V7.2H0V5.33L7.2 0l7.198 5.328M6 8.641h2.398v7.199H6v-7.2m-4.8 0h2.402v7.2H1.199v-7.2m12 0v3.598l-2.398 2.594V8.641h2.398m-4.68 8.64L7.802 18l2.039 2.16H0v-2.879h8.52M18 15.84v4.32h-3.602v2.88L9.602 18l4.796-5.04v2.88zm0 0"
      />
    </Svg>
  );
};

export default Icon;
