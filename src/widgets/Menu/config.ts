export const links = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
    target: '',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange-africaswap.web.app/',
        target: ''
      },
      {
        label: 'Liquidity',
        href: 'https://exchange-africaswap.web.app/#/pool',
        target: ''
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
    target: '',
  },
  // {
  //   label: 'Nests',
  //   icon: 'PoolIcon',
  //   href: '/nests',
  // },
  // {
  //   label: 'Pools',
  //   icon: 'PoolIcon',
  //   href: '/pools',
  // },
  // {
  //   label: 'Lottery',
  //   icon: 'TicketIcon',
  //   href: '/lottery',
  // },
  // {
  //   label: 'NFT',
  //   icon: 'NftIcon',
  //   href: '/nft',
  // },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'Work In Progress',
        href: '/',
        target: ''
      },
      // {
      //   label: 'CoinGecko',
      //   href: 'https://www.coingecko.com/en/coins/goose-finance',
      // },
      // {
      //   label: 'CoinMarketCap',
      //   href: 'https://coinmarketcap.com/currencies/goose-finance/',
      // },
      // {
      //   label: 'AstroTools',
      //   href: 'https://app.astrotools.io/pancake-pair-explorer/0x19e7cbecdd23a16dfa5573df54d98f7caae03019',
      // },
    ],
  },
  {
    label: 'More',
    icon: 'MoreIcon',
    items: [
      {
        label: 'Github',
        href: '/',
        target: '_blank'
      },
      {
        label: 'Docs',
        href: 'https://africaswap.gitbook.io/africaswap/',
        target: '_blank'
      },
      {
        label: 'Blog',
        href: 'https://africaswap.medium.com/',
        target: '_blank'
      },
    ],
  },
  {
    label: 'Become a P2P gateway',
    icon: 'GooseIcon',
    items: [
      {
        label: 'How does it work?',
        href: '/',
        target: '_blank'
      },
      {
        label: 'Sign me up',
        href: 'https://forms.gle/5SLkbHoJRFuqseEJA',
        target: '_blank'
      },
    ],
  },
  {
    label: 'Security',
    icon: 'AuditIcon',
    items: [
      {
        label: 'Audit by Hacken (Goose)',
        href: 'https://www.goosedefi.com/files/hackenAudit.pdf',
        target: '_blank'
      },
      {
        label: 'Audit by CertiK (Goose)',
        href: 'https://certik.org/projects/goose-finance',
        target: '_blank'
      },
      {
        label: 'Code dif checker',
        href: '/',
        target: '_blank'
      },
    ],
  },
];

export const socials = [
  {
    label: "Telegram",
    icon: "TelegramIcon",
    items: [
      {
        label: "English",
        href: "/",
        // target: '_blank'
      },
      // {
      //   label: "Bahasa Indonesia",
      //   href: "https://t.me/PancakeSwapIndonesia",
      // },
      // {
      //   label: "中文",
      //   href: "https://t.me/goosefinancechinese",
      // },
      // {
      //   label: "Tiếng Việt",
      //   href: "https://t.me/PancakeSwapVN",
      // },
      // {
      //   label: "Italiano",
      //   href: "https://t.me/pancakeswap_ita",
      // },
      // {
      //   label: "русский",
      //   href: "https://t.me/goosefinancerussian",
      // },
      // {
      //   label: "Türkiye",
      //   href: "https://t.me/GooseFinanceTurkey",
      // },
      // {
      //   label: "Português",
      //   href: "https://t.me/PancakeSwapPortuguese",
      // },
      // {
      //   label: "Español",
      //   href: "https://t.me/goosefinancespanish",
      // },
      // {
      //   label: "日本語",
      //   href: "https://t.me/goosefinancejapanese",
      // },
      // {
      //   label: "Français",
      //   href: "https://t.me/pancakeswapfr",
      // },
      {
        label: "Announcements",
        href: "/",
        // target: '_blank'
      },
      {
        label: "Price Bot",
        href: "/",
        // target: '_blank'
      },
    ],
  },
  {
    label: "Twitter",
    icon: "TwitterIcon",
    href: "https://twitter.com/AfricaSwapAFX",
    target: '_blank'
  },
];

export const MENU_HEIGHT = 64;
export const MENU_ENTRY_HEIGHT = 48;
export const SIDEBAR_WIDTH_FULL = 240;
export const SIDEBAR_WIDTH_REDUCED = 56;
